<page>
   <body>
   <div style="margin-top: 30px; padding: 80px; width: 100%;">
      <img width="200" src="http://uploads.webflow.com/574d727cf4830ad21e5afe0d/574d72b3d9474ad51c689d4a_logo.png">
      <br><br><br><br>
         <table cellspacing="0" style="width: 100%;">
           <tr>
               <td style="width: 50%;">
                  <p><?php echo $_POST['CompanyName']; ?>
                     <br><?php echo $_POST['ContactPersonName']; ?>
                     <br><?php echo $_POST['MailBox']; ?>
                     <br><?php echo $_POST['ZipCode']; ?> <?php echo $_POST['City']; ?>
                  </p>
               </td>
               <td style="width: 50%;">
                  <p><strong><?php echo $_POST['SenderName']; ?></strong>
                     <br><?php echo $_POST['SenderStreet']; ?>
                     <br><?php echo $_POST['SenderZipCode']; ?> <?php echo $_POST['SenderCity']; ?>
                     <br>
                     <a href=""><?php echo $_POST['SenderWebsite']; ?></a>
                  </p>
               </td>
           </tr>
         </table>
         <br><br><br>
         <table cellspacing="0" style="width: 100%;">
           <tr>
               <td style="width: 50%;">
                  <p>Factuurdatum: <?php echo date('m/d/Y'); ?>
                  <br>Vervaldatum: <?php echo $_POST['ExpireDate']; ?><br>
                  Factuurnummer: <?php echo $_POST['InvoiceNumber']; ?><br>
                  Kenmerk: <?php echo $_POST['Identification']; ?><br
                  >Klantnummer: <?php echo $_POST['CompanyNumber']; ?></p>
               </td>
           </tr>
         </table>
         <br><br>
         <table cellspacing="0" style="width: 100%;">
            <tr style="background-color: #ccc;">
               <td style="width: 10%;">
                  Aantal
               </td>
               <td style="width: 40%;">
                  Omschrijving
               </td>
               <td style="width: 15%;">
                  Prijs per eenheid
               </td>
               <td style="width: 10%;">
                  BTW
               </td>
               <td style="width: 10%;">
                  Totaal (Euro)
               </td>
            </tr><br><br><br><br>
            <?php 
	$orderlines = json_decode($_POST['OrderLines'], TRUE);
	foreach($orderlines as $value) {
               echo '
               <tr>
                  <td style="width: 10%;">
                     '.$value['quantity'].'
                  </td>
                  <td style="width: 40%;">
                     '.$value['description'].'
                  </td>
                  <td style="width: 15%;">
                     €'.$value['price'].'
                  </td>
                  <td style="width: 10%;">
                     '.$value['taxRate'].'
                  </td>
                  <td style="width: 10%;">
                     €'.$value['total'].'
                  </td>
              </tr>';
            }
         
           ?>
         </table>
         <br><br>

	<table>	  
		<tr>
		  <td style="width: 75%;">
			Total price:
		  </td>
		  <td style="width: 15%;">
		   
		  </td>
		  <td style="width: 10%;">
			€
			<?php 
				$totalPrice = 0;
				foreach($orderlines as $value) {
			      		$totalPrice += floatval($value['total']);
				}
				echo $totalPrice;
			?>
		  </td>
	      </tr>
	</table>         
	<br><br>
         <div style="width: 550px;">
            <p><?php echo $_POST['Message'] ?></p>
         </div>
         <br><br><br>
         <table cellspacing="0" style="width: 600px;">
           <tr>
               <td style="width: 190px;">
                  <div>BTWnummer: <?php echo $_POST['KVK'] ?></div>
               </td>
               <td style="width: 210px;">
                  <div>BICnummer: <?php echo $_POST['BIC'] ?> <?php echo $_POST['IBAN'] ?></div>
               </td>
               <td style="width: 190px;">
                  <div>BTWnummer: <?php echo $_POST['BTW'] ?></div>
               </td>
           </tr>
         </table>
      </div>
   </body>
</page>
